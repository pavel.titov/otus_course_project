﻿using System;
using System.Collections.Generic;

namespace Project.lib.Models
{
    public class Account
    {
        public int AccountId { get; set; }
        public string Title { get; set; }
        public decimal Amount { get; set; }
        public int CurrencyId { get; set; }
        public Currency Currency { get; set; }
        public DateTime CreationDate { get; set; }
        public string Description { get;set; }
        public int UserId { get; set; }
        public List<Operation> Operations { get; set; }
    }
}
