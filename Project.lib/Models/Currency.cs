﻿namespace Project.lib.Models
{
    public class Currency
    {
        public int CurrencyId { get; set; }
        public string Code { get; set; }
        public char Sign { get; set; }
    }
}
