﻿using System;

namespace Project.lib.Models
{
    public class Target
    {
        public int TargetId { get; set; }
        public decimal Amount { get; set; }
        public int CurrencyId { get; set; }
        public Currency Currency { get; set; }
        public DateTime Date { get; set; }
        public string Description { get; set; }
        public int UserId { get; set; }
    }
}
