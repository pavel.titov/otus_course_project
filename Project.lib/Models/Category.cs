﻿namespace Project.lib.Models
{
    public class Category
    {
        public int CategoryId { get; set; }
        public int OperationTypeId { get; set; }
        //public OperationType OperationType { get; set; }
        public string Title { get; set; }
        public int? SubCategoryId { get; set; }
    }
}
