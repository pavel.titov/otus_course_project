﻿using System;

namespace Project.lib.Models
{
    public class Operation
    {
        public int OperationId { get; set; }
        public decimal Amount { get; set; }
        public DateTime Date { get; set; }
        public int CategoryId { get; set; }
        public Category Category { get; set; }
        public string Description { get; set; }
        public int AccountId { get; set; }
    }
}
