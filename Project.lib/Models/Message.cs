﻿namespace Project.lib.Models
{
    public class Message
    {
        public string Filter { get; private set; }
        public string IncludeProperties { get; private set; }

        public Message(string filter, string includeProperties)
        {
            Filter = filter;
            IncludeProperties = includeProperties;
        }
    }
}
