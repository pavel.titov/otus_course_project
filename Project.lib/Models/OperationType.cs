﻿using System.Collections.Generic;

namespace Project.lib.Models
{
    public class OperationType
    {
        public int OperationTypeId { get; set; }
        public string TypeTitle { get; set; }
        public List<Category> Categories { get; set; }
    }
}
