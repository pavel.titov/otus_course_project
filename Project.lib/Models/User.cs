﻿using System;
using System.Collections.Generic;

namespace Project.lib.Models
{
    public class User
    {
        public int UserId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Login { get; set; }
        public string Password { get; set; }
        public DateTime CreationDate { get; set; }
        public List<Account> Accounts { get; set; }
        public List<Target> Targets { get; set; }
    }
}
