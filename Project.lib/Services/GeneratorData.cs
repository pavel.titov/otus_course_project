﻿using Project.lib.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;


namespace Project.lib.Services
{
    public class GeneratorData : IDataGenerator<Operation>
    {
        private int _countElements;
        private int _countThread;

        public GeneratorData(int countElements, int countThread)
        {
            _countElements = countElements;
            _countThread = countThread;
        }
               
        public IEnumerable<Operation> Generate()
        {
            List<ThreadGenerator> listGen = new List<ThreadGenerator>();
            for (int i = 0; i < _countThread; i++)
            {
                listGen.Add(new ThreadGenerator(_countElements));
            }
            listGen.ForEach(n => n.Start());
            listGen.ForEach(n => n._thread.Join());

            List<Operation> listOperations = new();
            foreach (var item in listGen.Select(s => s.list).ToList())
            {
                listOperations.AddRange(item);
            }

            return listOperations;
        }
    }


    class ThreadGenerator
    {
        public List<Operation> list = new List<Operation>();
        public Thread _thread;
        private int _countElements;

        public ThreadGenerator(int countElements)
        {
            _countElements = countElements;
        }

        public void Start()
        {
            _thread = new Thread(GetOperations);
            _thread.Start();
        }

        private void GetOperations()
        {
            Random r = new Random();
            for (int i = 0; i < _countElements; i++)
            {
                list.Add( new Operation()
                {
                    AccountId = r.Next(1, 9),
                    Amount = r.Next(1, 151),
                    CategoryId = r.Next(1, 23),
                    Date = DateTime.Now.AddDays(-r.Next(0, 61))
                });
            }
        }
    }
}
