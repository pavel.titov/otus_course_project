﻿using System.Security.Cryptography;
using System.Text;

namespace Project.lib.Services
{
    public static class SHA
    {
        public static string Generate(string password)
        {
            using SHA256 sHA256 = SHA256.Create();
            byte[] shaBytes = sHA256.ComputeHash(Encoding.UTF8.GetBytes(password));

            StringBuilder builder = new();
            for (int i = 0; i < shaBytes.Length; i++)
            {
                builder.Append(shaBytes[i].ToString("x2"));
            }
            return builder.ToString();
        }
    }
}
