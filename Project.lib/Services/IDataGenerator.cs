﻿using Project.lib.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project.lib.Services
{
    public interface IDataGenerator<T>
    {
        IEnumerable<T> Generate();
    }
}
