﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Net.Http.Json;
using System.Text.Json;
using System.Threading.Tasks;
using Project.lib.Models;

namespace Client
{
    class ViewModel
    {
        private HttpClient _client;

        public ViewModel()
        {
            Init();
        }

        private void Init()
        {
            _client = new HttpClient();
            _client.BaseAddress = new Uri("https://localhost:44354/");
            _client.DefaultRequestHeaders.Accept.Clear();
            _client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

            _ = GetAsync<Currency>("Currencies").Result;
            _ = GetAsync<Category>("Categories").Result;
        }

        public async Task<ObservableCollection<T>> GetAsync<T>(string path)
        {
            HttpResponseMessage response = _client.GetAsync(path).Result;
            return response.IsSuccessStatusCode
                ? await response.Content.ReadFromJsonAsync<ObservableCollection<T>>()
                : new ObservableCollection<T>();
        }

        public async Task<ObservableCollection<T>> PostAsync<T>(string path, Message message)
        {
            HttpResponseMessage response = _client.PostAsJsonAsync(path, message).Result;
            return response.IsSuccessStatusCode
                ? await response.Content.ReadFromJsonAsync<ObservableCollection<T>>()
                : new ObservableCollection<T>();
        }

        public async Task<bool> PostUserAsync(string path, User user)
        {
            HttpResponseMessage response = _client.PostAsJsonAsync(path, user).Result;
            return response.IsSuccessStatusCode
                ? await response.Content.ReadFromJsonAsync<bool>()
                : false;
        }

        public void PostOperation(string path, Operation operation)
        {
            HttpResponseMessage response = _client.PostAsJsonAsync(path, operation).Result;
        }

        public void PostAccount(string path, Account account)
        {
            HttpResponseMessage response = _client.PostAsJsonAsync(path, account).Result;
        }
    }
}
