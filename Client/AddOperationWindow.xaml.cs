﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Project.lib.Models;

namespace Client
{
    /// <summary>
    /// Логика взаимодействия для AddOperationWindow.xaml
    /// </summary>
    public partial class AddOperationWindow : Window
    {
        public Operation Operation { get; set; }
        public Account Account { get; set; }

        public AddOperationWindow()
        {
            InitializeComponent();
            //cbOperationType.ItemsSource = (ObservableCollection<OperationType>)DataContext;
        }

        private void btnAdd_Click(object sender, RoutedEventArgs e)
        {
            ChangeCategoriesVisible(Visibility.Collapsed);
            ChangeAddOperationsVisible(Visibility.Visible);
        }

        private void btnBack_Click(object sender, RoutedEventArgs e)
        {
            ChangeCategoriesVisible(Visibility.Visible);
            ChangeAddOperationsVisible(Visibility.Collapsed);
        }

        private void ChangeCategoriesVisible(Visibility visibility)
        {
            cbOperationType.Visibility = visibility;
            lbCategories.Visibility = visibility;
            btnAdd.Visibility = visibility;
        }

        private void ChangeAddOperationsVisible(Visibility visibility)
        {
            btnBack.Visibility = visibility;
            cbAccounts.Visibility = visibility;
            gridOperations.Visibility = visibility;
            btnSave.Visibility = visibility;
        }

        private void btnSave_Click(object sender, RoutedEventArgs e)
        {
            Operation = new Operation
            {
                Amount = decimal.Parse(tbAmount.Text),
                Description = tbDescription.Text,
                AccountId = ((Account)cbAccounts.SelectedItem).AccountId,
                Category = (Category)lbCategories.SelectedItem,
                CategoryId = ((Category)lbCategories.SelectedItem).CategoryId,
                Date = DateTime.Now
            };
            Account = (Account)cbAccounts.SelectedItem;
            this.Close();
        }
    }
}
