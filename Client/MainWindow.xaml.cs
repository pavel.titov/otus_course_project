﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using SqlDataAccess;
using Microsoft.EntityFrameworkCore;
using Project.lib.Models;
using Project.lib.Services;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text.Json;
using System.Net.Http.Json;

namespace Client
{
    public partial class MainWindow
    {
        private ViewModel _viewModel;

        public MainWindow()
        {
            InitializeComponent();

            foreach (var item in tabControl.Items)
            {
                (item as TabItem).Visibility = Visibility.Collapsed;
            }
        }

        private void btnAddOperation_Click(object sender, RoutedEventArgs e)
        {
            AddOperationWindow opWindow = new AddOperationWindow();
            var ListOfOperationTypes = _viewModel.GetAsync<OperationType>("operationtypes").Result.Take(2).ToList();

            opWindow.cbOperationType.ItemsSource = ListOfOperationTypes;
            ListOfOperationTypes.Add(new OperationType { OperationTypeId = 3, TypeTitle = "Перевод" });
            opWindow.cbOperationType.DisplayMemberPath = "TypeTitle";
            opWindow.cbOperationType.SelectedIndex = 0;

            opWindow.cbAccounts.ItemsSource = lbAccounts.Items;
            opWindow.ShowDialog();
            var operation = opWindow.Operation;
            _viewModel.PostOperation(@"Operations\AddUser", operation);
            datePicker.SelectedDate = DateTime.Now;
            var account = opWindow.Account;
            account.Operations.Add(operation);
            account.Amount -= operation.Amount;
            //_viewModel.PostAccount(@"Accounts\UpdateAccount", account);
        }

        private void DatePicker_SelectedDateChanged(object sender, SelectionChangedEventArgs e)
        {
            if (lbCurrentDateOperations != null)
            {
                lbCurrentDateOperations.ItemsSource = _viewModel.GetAsync<Operation>("Operations").Result
                    .Where(d => d.Date.Date == ((DatePicker)sender).SelectedDate.Value.Date);
            }
        }

        private void btnEnter_Click(object sender, RoutedEventArgs e)
        {
            _viewModel = new ViewModel();
            bool check = _viewModel.PostUserAsync("validation\\GetUserLogged",
                new User { Login = tbLogin.Text, Password = SHA.Generate(tbPassword.Password) }).Result;
            if (check)
            {
                foreach (var item in tabControl.Items)
                {
                    if((item as TabItem).Name != "tabItemLogin")
                    {
                        (item as TabItem).Visibility = Visibility.Visible;
                    }
                }
                tabControl.SelectedIndex = 1;


                _ = _viewModel.GetAsync<Currency>("Currencies").Result;
                _ = _viewModel.GetAsync<Category>("Categories").Result;
                var users = _viewModel.PostAsync<User>(@"Users\GetUser", new Message(tbLogin.Text, "Accounts.Operations,Targets")).Result;
                lbAccounts.ItemsSource = users.First().Accounts;
                datePicker.SelectedDate = DateTime.Now;
            }
            else
            {
                MessageBox.Show("Не верный логин/пароль");
            }
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            _ = _viewModel.GetAsync<Operation>("Operations\\GenOperation");
        }
    }
}
