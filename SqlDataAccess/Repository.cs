﻿using Microsoft.EntityFrameworkCore;
using Project.lib.Services;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Linq.Expressions;

namespace SqlDataAccess
{
    public class Repository<TEntity> : IRepository<TEntity> where TEntity : class
    {
        private readonly MoneyBoxContext _context;
        private readonly DbSet<TEntity> _dbSet;

        public Repository(MoneyBoxContext context)
        {
            _context = context ?? throw new ArgumentNullException(nameof(context));
            _dbSet = context.Set<TEntity>();
        }

        public void Add(TEntity entity)
        {
            _context.Set<TEntity>().Add(entity);
            _context.SaveChanges();
        }

        public void Update(TEntity entity)
        {
            _context.Set<TEntity>().Update(entity);
            _context.SaveChanges();
        }

        public IEnumerable<TEntity> GetAll()
        {
            _context.Set<TEntity>().Load();
            return _context.Set<TEntity>().Local.ToObservableCollection();
        }

        public IEnumerable<TEntity> Get(Expression<Func<TEntity, bool>> filter = null, string includeProperties = "")
        {
            IQueryable<TEntity> query = _dbSet;
            if (filter != null)
            {
                query = query.Where(filter);
            }
            foreach (string includeProperty in includeProperties.Split(',', StringSplitOptions.RemoveEmptyEntries))
            {
                query = query.Include(includeProperty);
            }
            return new ObservableCollection<TEntity>(query.ToList());
        }

        public void Remove(TEntity entity)
        {
            _context.Remove(entity);
            _context.SaveChanges();
        }

        /// <summary>
        /// Добавить список данных в БД
        /// </summary>
        /// <param name="range"></param>
        public void AddRange(IEnumerable<TEntity> range)
        {
            _context.AddRange(range);
            _context.SaveChanges();
        }
    }
}
