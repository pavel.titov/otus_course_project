SELECT main."CategoryId", ot."TypeTitle", main."Title" AS "Category", COALESCE(sub."Title", main."Title") AS "Main Category"
FROM "Categories" AS main
LEFT JOIN "Categories" AS sub ON main."SubCategoryId" = sub."CategoryId"
JOIN "OperationTypes" AS ot ON main."OperationTypeId" = ot."OperationTypeId"
ORDER BY main."CategoryId";