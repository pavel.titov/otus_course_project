﻿CREATE SEQUENCE IF NOT EXISTS users_id_seq;
CREATE TABLE IF NOT EXISTS users 
(
	id  INT  NOT NULL    DEFAULT NEXTVAL('users_id_seq'), 
 	first_name      CHARACTER VARYING(255)      NOT NULL, 
	second_name      CHARACTER VARYING(255)      NOT NULL, 
	login      CHARACTER VARYING(255)      NOT NULL, 
	password  CHARACTER VARYING(255)      NOT NULL,
	creation_date date NOT NULL,
	CONSTRAINT users_pkey PRIMARY KEY (id)
);

CREATE SEQUENCE IF NOT EXISTS accounts_id_seq;
CREATE TABLE IF NOT EXISTS accounts 
(
	id  INT  NOT NULL    DEFAULT NEXTVAL('accounts_id_seq'), 
 	account_name      CHARACTER VARYING(255)      NOT NULL, 
	amount       money     NOT NULL, 
	description CHARACTER VARYING(255),
	creation_date date NOT NULL,
	user_id INT  NOT NULL,
	currency_id int,
	CONSTRAINT accounts_pkey PRIMARY KEY (id),
	CONSTRAINT account_fk_user_id FOREIGN KEY (account_id) REFERENCES users(id) ON DELETE CASCADE,
	CONSTRAINT account_currency_fk_currency_id FOREIGN KEY (currency_id) REFERENCES currency(id) ON DELETE CASCADE
);


CREATE SEQUENCE public.transactions_id_seq;
CREATE TABLE public.transactions
(
    id integer NOT NULL DEFAULT nextval('transactions_id_seq'::regclass),
    amount money NOT NULL,
    description character varying(255) COLLATE pg_catalog."default",
    creation_date date NOT NULL,
    account_id integer NOT NULL,
	kind_of_operation_id integer,
    category_id integer,
    CONSTRAINT transactions_pkey PRIMARY KEY (id),
    
	CONSTRAINT transaction_fk_account_id FOREIGN KEY (account_id)
    REFERENCES public.accounts (id) MATCH SIMPLE
	ON UPDATE NO ACTION ON DELETE CASCADE,
	
	CONSTRAINT transaction_fk_kind_of_operation FOREIGN KEY (kind_of_operation_id)
    REFERENCES public.kind_of_operations (id) MATCH SIMPLE
	ON UPDATE NO ACTION ON DELETE CASCADE,
	
	CONSTRAINT transaction_fk_category FOREIGN KEY (category_id)
    REFERENCES public.categories (id) MATCH SIMPLE
	ON UPDATE NO ACTION ON DELETE CASCADE
);

CREATE SEQUENCE public.targets_id_seq;
CREATE TABLE public.targets
(
    id integer NOT NULL DEFAULT nextval('targets_id_seq'::regclass),
    description character varying(255),
    target_amount money NOT NULL,
    target_date date NOT NULL,
    CONSTRAINT targets_pkey PRIMARY KEY (id)
);


CREATE SEQUENCE IF NOT EXISTS currency_id_seq;
CREATE TABLE IF NOT EXISTS currencies 
(
	id  INT  NOT NULL    DEFAULT NEXTVAL('currency_id_seq'), 
 	currency_name      CHARACTER VARYING(3)      NOT NULL, 
	currency_country CHARACTER VARYING(255)      NOT NULL,
	currency_letter       money     NOT NULL,
	CONSTRAINT currencies_pkey PRIMARY KEY (id)
);

CREATE SEQUENCE public.categories_id_seq;
CREATE TABLE IF NOT EXISTS categories
(
	id  INT  NOT NULL    DEFAULT NEXTVAL('categories_id_seq'), 
 	category_name    CHARACTER VARYING(255)      NOT NULL, 
	category_description CHARACTER VARYING(255)      NOT NULL,
	CONSTRAINT categories_pkey PRIMARY KEY (id)
);

CREATE SEQUENCE public.kind_of_operations_id_seq;
CREATE TABLE IF NOT EXISTS kind_of_operations
(
	id  INT  NOT NULL    DEFAULT NEXTVAL('kind_of_operations_id_seq'), 
 	operation_name    CHARACTER VARYING(255)      NOT NULL, 
	operation_description CHARACTER VARYING(255)      NOT NULL,
	CONSTRAINT kind_of_operations_pkey PRIMARY KEY (id)
);
