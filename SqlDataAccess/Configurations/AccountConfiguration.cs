﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Project.lib.Models;

namespace SqlDataAccess.Configurations
{
    class AccountConfiguration : IEntityTypeConfiguration<Account>
    {
        public void Configure(EntityTypeBuilder<Account> builder)
        {
            builder.Property(ti => ti.Title).HasMaxLength(64).IsRequired();
            builder.HasData(new Account[]
            {
                new Account { AccountId = 1, Amount = 100000, CreationDate = DateTime.Now, Title="Cash",
                    Description="Money on the pocket", UserId = 1, CurrencyId = 1 },
                new Account { AccountId = 2, Amount = 2000, CreationDate = DateTime.Now, Title="Альфа",
                    Description="Дебетовая карта", UserId = 1, CurrencyId = 1 },
                new Account { AccountId = 3, Amount = 33000, CreationDate = DateTime.Now, Title="ВТБ",
                    Description="Дебетовая карта", UserId = 1, CurrencyId = 1 },
                new Account { AccountId = 4, Amount = 200234, CreationDate = DateTime.Now, Title="Сбербанк",
                    Description="Кредитная карта", UserId = 1, CurrencyId = 1 },
                new Account { AccountId = 5, Amount = 234200, CreationDate = DateTime.Now, Title="Тинькофф",
                    Description="Дебетовая карта", UserId = 1, CurrencyId = 2 },
                new Account { AccountId = 6, Amount = 224500, CreationDate = DateTime.Now, Title="Альфа",
                    Description="Кредитная карта", UserId = 2, CurrencyId = 3 },
                new Account { AccountId = 7, Amount = 456200, CreationDate = DateTime.Now, Title="Сбербанк",
                    Description="Дебетовая карта", UserId = 2, CurrencyId = 1 },
                new Account { AccountId = 8, Amount = 6200, CreationDate = DateTime.Now, Title="Наличные",
                    Description="Наличные", UserId = 2, CurrencyId = 1 },
            });
        }
    }
}
