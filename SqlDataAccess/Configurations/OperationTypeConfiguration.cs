﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Project.lib.Models;

namespace SqlDataAccess.Configurations
{
    class OperationTypeConfiguration : IEntityTypeConfiguration<OperationType>
    {
        public void Configure(EntityTypeBuilder<OperationType> builder)
        {
            builder.Property(t => t.TypeTitle).HasMaxLength(16).IsRequired();
            builder.HasData(new OperationType[]
            {
                new OperationType { OperationTypeId = 1, TypeTitle = "Расход" },
                new OperationType { OperationTypeId = 2, TypeTitle = "Доход" },
                new OperationType { OperationTypeId = 3, TypeTitle = "Пополнение" },
                new OperationType { OperationTypeId = 4, TypeTitle = "Списание" }
            });
        }
    }
}
