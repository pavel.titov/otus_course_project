﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Project.lib.Models;

namespace SqlDataAccess
{
    internal class TargetConfiguration : IEntityTypeConfiguration<Target>
    {
        public void Configure(EntityTypeBuilder<Target> builder)
        {
            builder.HasData(new Target[]
            {
                new Target { TargetId = 1, Amount = 1000000, CurrencyId = 3, 
                    Date=System.DateTime.Now, Description = "На карманные расходы", UserId = 1 },
                new Target { TargetId = 2, Amount = 3000000, CurrencyId = 3,
                    Date=System.DateTime.Now, Description = "На блекджек", UserId = 2 }
            });
        }
    }
}