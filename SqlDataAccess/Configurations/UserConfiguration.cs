﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Project.lib.Models;

namespace SqlDataAccess.Configurations
{
    class UserConfiguration : IEntityTypeConfiguration<User>
    {
        public void Configure(EntityTypeBuilder<User> builder)
        {
            builder.HasAlternateKey(l => l.Login);
            builder.Property(fn => fn.FirstName).HasMaxLength(32).IsRequired();
            builder.Property(ln => ln.LastName).HasMaxLength(32).IsRequired();
            builder.Property(l => l.Login).HasMaxLength(32).IsRequired();
            builder.Property(p => p.Password).HasMaxLength(256).IsRequired();
            builder.HasData(new User[]
            {
                new User { UserId = 1, FirstName="Иван", LastName="Факов", Login="Otus", Password="5994471abb01112afcc18159f6cc74b4f511b99806da59b3caf5a9c173cacfc5", CreationDate=DateTime.Now },
                new User { UserId = 2, FirstName="Меригуан", LastName="Чойболсан", Login="PinkWater", Password="65e84be33532fb784c48129675f9eff3a682b27168c0ea744b2cf58ee02337c5", CreationDate=DateTime.Now }
            });
        }
    }
}
