﻿using Microsoft.EntityFrameworkCore;
using Project.lib.Models;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace SqlDataAccess.Configurations
{
    class CategoryConfiguration : IEntityTypeConfiguration<Category>
    {
        public void Configure(EntityTypeBuilder<Category> builder)
        {
            builder.Property(c => c.Title).HasMaxLength(32).IsRequired();
            builder.HasData(new Category[]
            {
                new Category { CategoryId = 1, Title = "Еда", OperationTypeId = 1 },
                new Category { CategoryId = 2, Title = "Питание", OperationTypeId = 1 },
                new Category { CategoryId = 3, Title = "Кафе", SubCategoryId = 2, OperationTypeId = 1 },
                new Category { CategoryId = 4, Title = "Бар", SubCategoryId = 2, OperationTypeId = 1 },
                new Category { CategoryId = 5, Title = "Столовая", SubCategoryId = 2, OperationTypeId = 1},
                new Category { CategoryId = 6, Title = "Автомобиль", OperationTypeId = 1},
                new Category { CategoryId = 7, Title = "Парковка", SubCategoryId = 6, OperationTypeId = 1 },
                new Category { CategoryId = 8, Title = "Топливо", SubCategoryId = 6, OperationTypeId = 1 },
                new Category { CategoryId = 9, Title = "Мойка", SubCategoryId = 6, OperationTypeId = 1 },
                new Category { CategoryId = 10, Title = "Ремонт", SubCategoryId = 6, OperationTypeId = 1 },
                new Category { CategoryId = 11, Title = "Спорт", OperationTypeId = 1 },
                new Category { CategoryId = 12, Title = "Транспорт", OperationTypeId = 1 },
                new Category { CategoryId = 13, Title = "Метро", SubCategoryId = 12, OperationTypeId = 1 },
                new Category { CategoryId = 14, Title = "Автобус", SubCategoryId = 12, OperationTypeId = 1 },
                new Category { CategoryId = 15, Title = "Электричка", SubCategoryId = 12, OperationTypeId = 1 },
                new Category { CategoryId = 16, Title = "Такси", SubCategoryId = 12, OperationTypeId = 1 },
                new Category { CategoryId = 17, Title = "Зарплата", OperationTypeId = 2 },
                new Category { CategoryId = 18, Title = "Процент по вкладам", OperationTypeId = 2 },
                new Category { CategoryId = 19, Title = "Кешбэк", OperationTypeId = 2 },
                new Category { CategoryId = 20, Title = "Аванс", OperationTypeId = 2 },
                new Category { CategoryId = 21, Title = "Приход", OperationTypeId = 3 },
                new Category { CategoryId = 22, Title = "Уход", OperationTypeId = 4 },
            });
        }
    }
}
