﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Project.lib.Models;

namespace SqlDataAccess.Configurations
{
    class CurrencyConfiguration : IEntityTypeConfiguration<Currency>
    {
        public void Configure(EntityTypeBuilder<Currency> builder)
        {
            builder.Property(c => c.Code).HasMaxLength(3).IsRequired();
            builder.HasData(new Currency[]
            {
                new Currency { CurrencyId=1, Code="RUB", Sign='\u20BD' },
                new Currency { CurrencyId=2, Code="USD", Sign='\u0024' },
                new Currency { CurrencyId=3, Code="EUR", Sign='\u20AC' }
            });
        }
    }
}
