﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Project.lib.Models;

namespace SqlDataAccess.Configurations
{
    class OperationConfiguration : IEntityTypeConfiguration<Operation>
    {
        public void Configure(EntityTypeBuilder<Operation> builder)
        {
            builder.HasData(new Operation[]
            {
                new Operation { OperationId = 1, Amount = 20, Date = DateTime.Now.AddDays(-1), AccountId = 1, CategoryId = 1 },
                new Operation { OperationId = 2, Amount = 15, Date = DateTime.Now.AddDays(-1), AccountId = 2, CategoryId = 1 },
                new Operation { OperationId = 3, Amount = 11, Date = DateTime.Now.AddDays(-3), AccountId = 2, CategoryId = 3 },
                new Operation { OperationId = 4, Amount = 12, Date = DateTime.Now.AddDays(-2), AccountId = 2, CategoryId = 3 },
                new Operation { OperationId = 5, Amount = 1, Date = DateTime.Now.AddDays(-1), AccountId = 1, CategoryId = 3 },
                new Operation { OperationId = 6, Amount = 5, Date = DateTime.Now, AccountId = 1, CategoryId = 3 },
                new Operation { OperationId = 7, Amount = 17, Date = DateTime.Now.AddDays(-1), AccountId = 1, CategoryId = 4 },
                new Operation { OperationId = 8, Amount = 20, Date = DateTime.Now, AccountId = 1, CategoryId = 5 },
                new Operation { OperationId = 9, Amount = 30, Date = DateTime.Now, AccountId = 2, CategoryId = 6 },
                new Operation { OperationId = 10, Amount = 40, Date = DateTime.Now.AddDays(-1), AccountId = 1, CategoryId = 3 },
                new Operation { OperationId = 11, Amount = 50, Date = DateTime.Now.AddDays(-1), AccountId = 1, CategoryId = 19 },
                new Operation { OperationId = 12, Amount = 8, Date = DateTime.Now.AddDays(-1), AccountId = 2, CategoryId = 2 },
                new Operation { OperationId = 13, Amount = 29, Date = DateTime.Now.AddDays(-1), AccountId = 1, CategoryId = 11 },
                new Operation { OperationId = 14, Amount = 39, Date = DateTime.Now, AccountId = 1, CategoryId = 10 },
                new Operation { OperationId = 15, Amount = 58, Date = DateTime.Now, AccountId = 2, CategoryId = 20 },
                new Operation { OperationId = 16, Amount = 34, Date = DateTime.Now, AccountId = 2, CategoryId = 18 },
                new Operation { OperationId = 17, Amount = 23, Date = DateTime.Now, AccountId = 2, CategoryId = 16 },
                new Operation { OperationId = 18, Amount = 34, Date = DateTime.Now, AccountId = 1, CategoryId = 15 },
                new Operation { OperationId = 19, Amount = 45, Date = DateTime.Now.AddDays(-2), AccountId = 1, CategoryId = 14 },
                new Operation { OperationId = 20, Amount = 56, Date = DateTime.Now.AddDays(-2), AccountId = 2, CategoryId = 12 },
                new Operation { OperationId = 21, Amount = 67, Date = DateTime.Now.AddDays(-2), AccountId = 1, CategoryId = 11 },
                new Operation { OperationId = 22, Amount = 34, Date = DateTime.Now, AccountId = 1, CategoryId = 12 },
                new Operation { OperationId = 23, Amount = 22, Date = DateTime.Now, AccountId = 1, CategoryId = 8 },
                new Operation { OperationId = 24, Amount = 11, Date = DateTime.Now, AccountId = 1, CategoryId = 7 },
                new Operation { OperationId = 25, Amount = 15, Date = DateTime.Now, AccountId = 1, CategoryId = 6 },
            });

            //builder.Property(c => c.Category).IsRequired();
        }
    }
}
