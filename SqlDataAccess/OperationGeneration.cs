﻿using Project.lib.Models;
using Project.lib.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SqlDataAccess
{
    public class OperationGeneration
    {
        private Repository<Operation> _repo;

        public OperationGeneration(Repository<Operation> repo)
        {
            _repo = repo;
        }

        public Task GenerateOperations()
        {
            GeneratorData generator = new GeneratorData(2000, 5);
            return Task.Run(() =>
            {
                _repo.AddRange(generator.Generate());
            });
        }
    }
}
