﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using System.IO;

namespace SqlDataAccess
{
    public class ContextFactory : IDbContextFactory<MoneyBoxContext>
    {
        public MoneyBoxContext CreateDbContext()
        {
            DbContextOptionsBuilder<MoneyBoxContext> optionsBuilder = new();

            ConfigurationBuilder builder = new();
            builder.SetBasePath(Directory.GetCurrentDirectory());
            builder.AddJsonFile("appsettings.json");
            IConfiguration config = builder.Build();

            string connectionString = config.GetConnectionString("DefaultConnection");
            optionsBuilder.UseNpgsql(connectionString);
            return new MoneyBoxContext(optionsBuilder.Options);
        }
    }
}
