﻿using Project.lib.Models;
using System;

namespace SqlDataAccess
{
    public class UnitOfWork : IDisposable
    {
        private MoneyBoxContext _context;

        private Repository<User> _userRepository;
        private Repository<Account> _accountRepository;
        private Repository<Operation> _operationRepository;
        private Repository<Category> _categoryRepository;
        private Repository<Target> _targetRepository;
        private Repository<OperationType> _operationTypeRepository;
        private Repository<Currency> _currencyRepository;

        public Repository<User> UserRepository
        {
            get
            {
                if (_userRepository == null)
                {
                    _userRepository = new Repository<User>(_context);
                }
                return _userRepository;
            }
        }
        public Repository<Account> AccountRepository
        {
            get
            {
                if (_accountRepository == null)
                {
                    _accountRepository = new Repository<Account>(_context);
                }
                return _accountRepository;
            }
        }
        public Repository<Operation> OperationRepository
        {
            get
            {
                if (_operationRepository == null)
                {
                    _operationRepository = new Repository<Operation>(_context);
                }
                return _operationRepository;
            }
        }
        public Repository<Category> CategotyRepository
        {
            get
            {
                if (_categoryRepository == null)
                {
                    _categoryRepository = new Repository<Category>(_context);
                }
                return _categoryRepository;
            }
        }
        public Repository<Target> TargetRepository 
        {
            get 
            {
                if (_targetRepository == null)
                {
                    _targetRepository = new Repository<Target>(_context);
                }
                return _targetRepository;
            }
        }
        public Repository<OperationType> OperationTypeRepository 
        {
            get
            {
                if (_operationTypeRepository == null)
                {
                    _operationTypeRepository = new Repository<OperationType>(_context);
                }
                return _operationTypeRepository;
            }
        }
        public Repository<Currency> CurrencyRepository
        {
            get 
            { 
                if(_currencyRepository == null)
                {
                    _currencyRepository = new Repository<Currency>(_context);
                }
                return _currencyRepository;
            }
        }

        public UnitOfWork(MoneyBoxContext context)
        {
            _context = context;
        }

        public void Save()
        {
            _context.SaveChanges();
        }

        private bool _disposed;

        protected virtual void Dispose(bool disposing)
        {
            if (!_disposed)
            {
                if (disposing)
                {
                    _context.Dispose();
                }
            }
            _disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}
