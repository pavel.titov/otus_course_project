﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using SqlDataAccess;
using Project.lib.Models;

namespace WebApi.Controllers
{
    [Route("[controller]")]
    [ApiController]
    public class AccountsController : ControllerBase
    {
        private readonly UnitOfWork _unitOfWork;

        public AccountsController(UnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        [HttpPost("AddAccount")]
        public void Add(Account account)
        {
            _unitOfWork.AccountRepository.Add(account);
        }

        [HttpPost("UpdateAccount")]
        public void Update(Account account)
        {
            _unitOfWork.AccountRepository.Update(account);
        }

        [HttpGet]
        public IEnumerable<Account> GetAll()
        {
            return _unitOfWork.AccountRepository.GetAll();
        }

        [HttpGet("GetAccount")]
        public IEnumerable<Account> Get(Message message)
        {
            return _unitOfWork.AccountRepository.Get(ac => ac.Title == message.Filter, message.IncludeProperties);
        }

        [HttpPost("Remove")]
        public void Remove(Account account)
        {
            _unitOfWork.AccountRepository.Remove(account);
        }
    }
}
