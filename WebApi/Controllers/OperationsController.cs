﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using SqlDataAccess;
using Project.lib.Models;
using Project.lib.Services;

namespace WebApi.Controllers
{
    [Route("[controller]")]
    [ApiController]
    public class OperationsController : ControllerBase
    {
        private readonly UnitOfWork _unitOfWork;

        public OperationsController(UnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        [HttpPost("AddUser")]
        public void Add(Operation operation)
        {
            operation.Category = null;
            _unitOfWork.OperationRepository.Add(operation);
            _unitOfWork.Save();
        }

        [HttpGet]
        public IEnumerable<Operation> Get()
        {
            return _unitOfWork.OperationRepository.GetAll();
        }

        [HttpPost("Remove")]
        public void Remove(Operation operation)
        {
            _unitOfWork.OperationRepository.Remove(operation);
        }

        [HttpGet("GenOperation")]
        public async void GenerateDataAsync()
        {
            OperationGeneration OpGen = new(_unitOfWork.OperationRepository);
            await OpGen.GenerateOperations();
        }
    }
}
