﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using SqlDataAccess;
using Project.lib.Models;


namespace WebApi.Controllers
{
    [Route("[controller]")]
    [ApiController]
    public class CategoriesController : ControllerBase
    {
        private readonly UnitOfWork _unitOfWork;

        public CategoriesController(UnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        [HttpPost("Add")]
        public void Add(Category category)
        {
            _unitOfWork.CategotyRepository.Add(category);
        }

        [HttpGet]
        public IEnumerable<Category> Get()
        {
            return _unitOfWork.CategotyRepository.GetAll();
        }

        [HttpPost("Remove")]
        public void Remove(Category category)
        {
            _unitOfWork.CategotyRepository.Remove(category);
        }
    }
}
