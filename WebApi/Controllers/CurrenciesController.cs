﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using SqlDataAccess;
using Project.lib.Models;

namespace WebApi.Controllers
{
    [Route("[controller]")]
    [ApiController]
    public class CurrenciesController : ControllerBase
    {
        private readonly UnitOfWork _unitOfWork;

        public CurrenciesController(UnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        [HttpPost("Add")]
        public void Add(Currency currency)
        {
            _unitOfWork.CurrencyRepository.Add(currency);
        }

        [HttpGet]
        public IEnumerable<Currency> Get()
        {
            return _unitOfWork.CurrencyRepository.GetAll();
        }

        [HttpPost("Remove")]
        public void Remove(Currency currency)
        {
            _unitOfWork.CurrencyRepository.Remove(currency);
        }
    }
}
