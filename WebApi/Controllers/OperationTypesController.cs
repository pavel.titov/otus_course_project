﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using SqlDataAccess;
using Project.lib.Models;


namespace WebApi.Controllers
{
    [Route("[controller]")]
    [ApiController]
    public class OperationTypesController : ControllerBase
    {
        private readonly UnitOfWork _unitOfWork;

        public OperationTypesController(UnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        [HttpPost("Add")]
        public void Add(OperationType operationType)
        {
            _unitOfWork.OperationTypeRepository.Add(operationType);
        }

        [HttpGet]
        public IEnumerable<OperationType> Get()
        {
            return _unitOfWork.OperationTypeRepository.GetAll();
        }

        [HttpPost("Remove")]
        public void Remove(OperationType operationType)
        {
            _unitOfWork.OperationTypeRepository.Remove(operationType);
        }
    }
}
