﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using SqlDataAccess;
using Project.lib.Models;

namespace WebApi.Controllers
{
    [Route("[controller]")]
    [ApiController]
    public class ValidationController : ControllerBase
    {
        private readonly UnitOfWork _unitOfWork;

        public ValidationController(UnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        [HttpPost("GetUserLogged")]
        public bool Get(User user)
        {
            return new Validation(_unitOfWork).IsUserLogged(user);
        }
    }
}
