﻿using Microsoft.AspNetCore.Mvc;
using Project.lib.Models;
using SqlDataAccess;
using System.Collections.Generic;

namespace WebApi.Controllers
{
    [Route("[controller]")]
    [ApiController]
    public class UsersController : ControllerBase
    {
        private readonly UnitOfWork _unitOfWork;

        public UsersController(UnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        [HttpPost("Add")]
        public void Add(User user)
        {
            _unitOfWork.UserRepository.Add(user);
        }

        [HttpGet("")]
        public IEnumerable<User> Get()
        {
            return _unitOfWork.UserRepository.GetAll();
        }

        [HttpPost("GetUser")]
        public IEnumerable<User> Get(Message message)
        {
            return _unitOfWork.UserRepository.Get(u => u.Login == message.Filter, message.IncludeProperties);
        }

        [HttpPost("Remove")]
        public void Remove(User user)
        {
            _unitOfWork.UserRepository.Remove(user);
        }
    }
}
