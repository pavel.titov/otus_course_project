﻿using Project.lib.Models;
using SqlDataAccess;
using System.Linq;

namespace WebApi
{
    public class Validation
    {
        private readonly UnitOfWork _unitOfWork;

        public Validation(UnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        public bool IsUserLogged(User user)
        {
            return _unitOfWork.UserRepository.Get(u => u.Login == user.Login && u.Password == user.Password).Any();
        }
    }
}
